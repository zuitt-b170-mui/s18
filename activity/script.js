let pokemon = {
    name:"Pikachu",
    level:3,
    health:100,
    attack:50,
    tackle: function(){
        console.log("This pokemon tackled targetPokemon")
        console.log("targetPokemon's health is now reduced to targetPokemonHealth")
    }
}

let trainer = {
    name:"Ash Ketchum",
    age: 16,
    
}

function Pokemon(name,level){
    this.name = name,
    this.level = level,
    this.health = 2*level,
    this.attack = level
    this.tackle = function(target){
        console.log(this.name + " tackled" + target.name);
        console.log("targetPokemon's health is now reduced to targetPokemonHealth");
    }
    this.faint = function(){
        console.log(this.name + " fainted.")
    }
}

let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon ("Charizard", 8);
let Bulbasaur = new Pokemon ("Bulbasaur", 9);