let grades = [98.5, 94.3, 89.2, 90.0];
console.log(grades);


// Objects - collection of related data and functionalities; usually representing real-world objects


// curly braces - initializer for creating objects
// key-value pair
// key - field/identifier to describe the data/value;
// value - info that serves as the value of the key;

let grade = {
    math:98.5,
    english:94.3,
    science:90.0,
    MAPEH:89.2,
};
console.log(grade);

// accessing elements inside object

/* 


*/

console.log(grade.english);

// accessing two keys in the object
console.log(grade.english, grade.math);

/* 
create a cellphone object that has the ff. keys (supply your own values for each)
    brand
    color
    mfd
take a ss of your output and sent it to our google chat
*/

let cellphone = {
    brand:"Apple",
    color:"White",
    mfd:2021
};

console.log(cellphone)
console.log(typeof cellphone);

let student = {
    firstName: "John",
    lastName: "Smith",
    mobileNo: 09178541607,
    location:{
        city:"Tokyo",
        country:"Japan",
    },
    email:["john@mail.com", "john.smith@mail.com"],

    fullName:(function name(fullName){
        // this - refers to the object where it is inserted
        /* 
            in this case, the code below could be typed as:
                console.log(this.firstName + " " + this.lastName);
        */
        console.log(this.firstName + " " + this.lastName);
    })
};

student.fullName();
console.log(student.location.city);
console.log(student.email[0]);
console.log(student.email[1]);

/* 
ARRAY OF OBJECTS
*/

let contactList = [
    {
        firstName:"John",
        lastName: "Smith",
        location: "Japan",
    },
    {
        firstName: "Jane",
        lastName: "Smith",
        location: "Japan"
    },
    {
        firstName: "Jasmine",
        lastName: "Smith",
        location: "Japan"
    },
]

// accessing an element inside the array
console.log(contactList[1]);
// accessing a key in an object that is inside the array
console.log(contactList[2].firstName);


/* 
Constructor Function - create a reusable function to create several objects
                     - useful for creating copies/instances of an object

Instance - concrete occurence of any object which emphasizes on the unique identity of it
*/



// Reusable Function/Object Method
function Laptop (name, manufacturedDate){
    this.name = name,
    this.manufacturedDate = manufacturedDate
};

let laptop1 = new Laptop("Lenovo",2008);
console.log(laptop1)

let laptop2 = new Laptop("Toshiba",1997);
console.log(laptop2)


let array = [laptop1,laptop2];
console.log(array);

/* 
initializing, adding, deleting, reassigning of object properties
*/

let car = {};

// adding object properties

car.name = "Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020;
console.log(car);

car.name = "Volvo";
console.log(car);

// delete car.manufacturedDate;
// console.log(car);


delete car["manufacturedDate"];
console.log(car);

let person = {
    name: "John",
    talk: function(){
        console.log("Hi! My name is " + this.name)
    }
};

console.log(person)


let friend = {
	firstName: "Joe",
	lastName: "Doe",
	address:{
		city: "Austin",
		state: "Texas"
	},
	emails:["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+ this.firstName + " " + this.lastName)
	}
};

let pokemon = {
    name:"Pikachu",
    level:3,
    health:100,
    attack:50,
    tackle: function(){
        console.log("This pokemon tackled targetPokemon")
        console.log("targetPokemon's health is now reduced to targetPokemonHealth")
    }
}

function Pokemon(name,level){
    this.name = name,
    this.level = level,
    this.health = 2*level,
    this.attack = level
    this.tackle = function(target){
        console.log(this.name + " tackled" + target.name);
        console.log("targetPokemon's health is now reduced to targetPokemonHealth");
    }
    this.faint = function(){
        console.log(this.name + " fainted.")
    }
}

let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon ("Charizard", 8);
let Bulbasaur = new Pokemon ("Bulbasaur", 9);
